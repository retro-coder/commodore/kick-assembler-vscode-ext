/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import { Provider, ProjectInfoProvider } from "./Provider";
import { AssemblerResults } from "../assembler/Assembler";
import {
    Connection,
	Diagnostic,
	DiagnosticSeverity,
} from "vscode-languageserver/node";


export default class DiagnosticProvider extends Provider {

    constructor(connection:Connection, projectInfo:ProjectInfoProvider) {
        super(connection, projectInfo);
    }

    /**
     * Does Something
     * @param uri 
     */
    public process(uri:string):void {

		//	for return
		const diagnostics:Diagnostic[] = [];
		
		//	grab the assembler results from the last change
		const results:AssemblerResults = this.getProjectInfo().getProject(uri).getAssemblerResults();
		
		//	create diagnostic for each error on the last compile
		if (results.assemblerInfo.getAssemblerErrors().length > 0) {

			//	only send back diagnostics for the
			//	file that is being worked on
			var fileNumber = -1;

			for (let file of results.assemblerInfo.getAssemblerFiles()) {
				if (file.isCurrent) {
					fileNumber = file.index;
					break;
				} 
			}			

			results.assemblerInfo.getAssemblerErrors().forEach((error) => {
				// console.log(error.range);
				if (error.range.fileIndex == fileNumber && (error.range.startLine > 0 && error.range.endLine > 0)) {
					diagnostics.push( {
						severity: error.level.toLowerCase() === 'warning' ? DiagnosticSeverity.Warning : DiagnosticSeverity.Error,
						range: {
							start: { line: error.range.startLine, character: error.range.startPosition },
							end: { line: error.range.endLine, character: error.range.endPosition},
						},
						message: error.message,
						source: "kickassembler",
					});
				}
			});
		}

        //	return the diagnostic information
		this.getConnection().sendDiagnostics({ uri, diagnostics });
	}

}