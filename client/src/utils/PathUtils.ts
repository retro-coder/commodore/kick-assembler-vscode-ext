/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import * as path
	from 'path';

import * as fs 
	from 'fs';

import { dirname } from 'path';
import { Uri } from "vscode"; 


export default class PathUtils {

	/**
	 * Converts "file:///d%3A/blaba/a.x" to "d:\blaba\a.x"
	 */
	public static uriToFileSystemPath(uri:string):string {
		let newuri = Uri.parse(uri);
		return newuri.fsPath;
	}

	/**
	 * Converts "file:///d%3A/blaba/a.x" to "d:\blaba\a.x"
	 */
    public static uriToPlatformPath(uri: string): string {
        let newuri = Uri.parse(uri);
        return newuri.fsPath;
	}
	
	/**
     * Returns the Path from a Filename
     * @param filename 
     */
    public static GetPathFromFilename(filename: string) {
        return dirname(filename);
	}
	
	/**
	 * Returns True if the File Exists
	 * 
	 * Accomodates filenames that might have forward
	 * slashes in thier name for posix transformations
	 * 
	 */
	public static fileExists(filename: string) {

		// account for forward slashes on non-windows platforms 
        // if (process.platform != "win32") 
		// 	filename = filename.replace("\\", "");

		try {
			fs.accessSync(filename, fs.constants.F_OK);
			return true;
		} catch(err) {
			return false;
		}
	}

	/*
		Returns True if the Directory Exists
	*/
	public static directoryExists(name: string) {

		try {
			fs.accessSync(name, fs.constants.F_OK);
			return true;
		} catch(err) {
			return false;
		}
	}

	/*
		Create Directory if None Exists
	*/
	public static directoryCreate(name: string) {

        if (!this.directoryExists(name)) {
			fs.mkdirSync(name)
        }

	}

	/*
		Remove File if it Exists
	*/
	public static fileRemove(name: string) {
		if (this.fileExists(name)) {
			fs.unlinkSync(name);
		}
	}

	public static getOutputPath(basePath: string, sourcePath: string, outputPath: string, keepSourceHierarchy = true) {

		if (!keepSourceHierarchy && outputPath.length <= 0) {
			outputPath = sourcePath;
		}

		basePath = path.normalize(basePath).trim();
		sourcePath = path.normalize(sourcePath).trim();
		outputPath = path.normalize(outputPath).trim();

		const splitBase = basePath.split(path.sep).filter(elm => elm);
		const splitSource = sourcePath.split(path.sep).filter(elm => elm);

		let matchPath = true;

		for (let i = 0; i < splitBase.length; i++) {

			const baseDir = splitBase[i];
			const sourceDir = splitSource[i];

			if (baseDir != sourceDir)
				matchPath = false;
		}

		if (!matchPath) {
			basePath = sourcePath;
			if (!path.isAbsolute(outputPath))
				outputPath = sourcePath;
		}

		const splitMatchBase = basePath.split(path.sep).filter(elm => elm);

		const subDirs: string[] = [];

		for (let i = splitMatchBase.length; i < splitSource.length; i++) {
			if (keepSourceHierarchy)
				subDirs.push(splitSource[i]);
		}

		if (path.isAbsolute(outputPath))
			basePath = "";

		const returnPath = path.resolve(basePath, outputPath, ...subDirs);

		return returnPath;
	}

}